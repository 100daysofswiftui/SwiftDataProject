//
//  SwiftDataProjectApp.swift
//  SwiftDataProject
//
//  Created by Pascal Hintze on 29.11.2023.
//

import SwiftUI

@main
struct SwiftDataProjectApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
