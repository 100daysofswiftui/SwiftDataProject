# SwiftDataProject

SwiftDataProject is a technique project used to explore SwiftData in more detail.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/swiftdata-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Edit SwiftData objects with SwiftUI
- Filter data using #Predicate
- NSPredicate
- Changing fetch requests dynamically
- Creating relationships
